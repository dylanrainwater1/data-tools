## Data Tools

This is just a bundle of tools to interact with data associated with the Wire app.

Cloud functions are in algolia/index, the rest are specialized tools to interact with the Endpoint, Firestore and Algolia APIs. 