/* Script meant to grab data from firebase store */
const firebase = require('firebase');
require('firebase/firestore');

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyARwXjdXxlUFbPQCYRpQfBKkNHwyaImPkY',
    authDomain: 'aggregation-app.firebaseapp.com',
    projectId: 'aggregation-app'
});
var db = firebase.firestore();

/* Fetches all articles from database. */
function get(endpoint, lim=100) {
    let sz = 0;
    let minsz = 999999999999;
    let maxsz = -1;

    let num_articles = 0;
    db.collection("articles").orderBy('published', 'desc').get().then((articles) => {
        articles.forEach((doc) => {
            let size = doc.data().content.length;
            num_articles++;
            sz += size;
            maxsz = Math.max(maxsz, size);
            minsz = Math.min(minsz, size);

            // console.log(size);
            // console.log(`${num_articles}. ${doc.id} => ${doc}`);

            if (doc.data().source === 'College of Arts & Sciences' && true) {
                let name = doc.id;
                db.collection('articles').doc(name).delete().then(() => {
                    console.log('Removed ' + name);
                }).catch((err) => {
                    console.log('Failed on ' + name);
                    console.log(err);
                });
            }

            if (doc.data().source === 'College of Arts & Sciences' && false) {
                let name = doc.id;
                db.collection('articles').doc(name).get().then((doc) => {
                    if (doc && doc.exists) {
                        let data = doc.data();
                        data.source = 'UT College of Arts & Sciences'
                        
                        let docId = `${data.source}-${name.split("-")[1]}`;
                        let docRef = db.collection('articles').doc(docId);
                        docRef.set(data).then(() => {
                            console.log(`Created ${docId}.`);
                            return true;
                        }).catch((err) => {
                            console.log(`Error saving ${docId}: ${err}`);
                            return false;
                        });
                    }
                });
            }
        });

        sz /= num_articles;
        // console.log("Average size: ", sz);
        // console.log("Max size: ", maxsz);
        // console.log("Min size: ", minsz);
    });
}

get('');