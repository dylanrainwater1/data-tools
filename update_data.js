/* Script meant to grab data from API endpoints and put into firebase store */
const fetch = require("node-fetch");
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const firebase = require('firebase');
require('firebase/firestore');

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyARwXjdXxlUFbPQCYRpQfBKkNHwyaImPkY',
    authDomain: 'aggregation-app.firebaseapp.com',
    projectId: 'aggregation-app'
});
var db = firebase.firestore();


/* API endpoints where news articles can be found
   All use JSON data, except UTM old which used XML.
*/
const endpoints = {
    "UT System" : "https://news.tennessee.edu/wp-json/wp/v2/posts",
    "UT Knoxville" : "https://news.utk.edu/wp-json/wp/v2/posts",
    "UT Chattanooga" : "https://blog.utc.edu/news/wp-json/wp/v2/posts",
    /*"UTM old" : "http://www.utm.edu/departments/univrel/archives/headlines_feed.php",*/
    "UT Martin" : "https://www.utm.edu/news/wp-json/wp/v2/posts",
    "UT Health Science Center" : "https://news.uthsc.edu/wp-json/wp/v2/posts", 
    "UT Institute of Agriculture": "https://utianews.tennessee.edu/wp-json/wp/v2/posts",
    "UT Institute for Public Service" : "http://ips.tennessee.edu/wp-json/wp/v2/posts",
    'UT Research Foundation': "https://utrf.tennessee.edu/wp-json/wp/v2/posts",
    'Alumnus': 'https://alumnus.tennessee.edu/wp-json/wp/v2/posts',
    'Torchbearer': 'https://torchbearer.utk.edu/wp-json/wp/v2/posts',
    'College of Education, Health, and Human Services': 'https://cehhs.utk.edu/wp-json/wp/v2/posts',
    'Tickle College of Engineering': 'https://tickle.utk.edu/wp-json/wp/v2/posts',
    'UT College of Architecture + Design': 'https://archdesign.utk.edu/wp-json/wp/v2/posts',
    'Nursing': 'https://nursing.utk.edu/wp-json/wp/v2/posts',
    'ArtSci': 'https://artsci.utk.edu/wp-json/wp/v2/posts',
    'Law': 'https://law.utk.edu/wp-json/wp/v2/posts',
    'Quest': 'https://quest.utk.edu/wp-json/wp/v2/posts',
    'Campus Scene': 'https://www.utm.edu/campusscene/wp-json/wp/v2/posts',
    'President': 'https://president.tennessee.edu/wp-json/wp/v2/posts'
};
const num_results = 100;
const args = `?per_page=${num_results}`;

const html_regex = /<.+?>/g;
const newline_regex = /\n/g;
function removeHTMLFromString(str) {
    return entities.decode(str.replace(html_regex, '')).replace(newline_regex, '\n');
}

let articles = [] // serves as in-memory collation of articles

/* Fetches every article (by using get()) */
function getall() {
    for (endpoint in endpoints) {
        get(endpoint);
    }    
}

function isTypeA(endpoint) {
    return endpoint === 'UT System' || 
           endpoint === 'Torchbearer' ||
           endpoint === 'Quest';
}

function isTypeB(endpoint) {
    return endpoint === 'IPS' ||
           endpoint === 'UTC' ||
           endpoint === 'Tickle' ||
           endpoint === 'ArchDesign' ||
           endpoint === 'Nursing' ||
           endpoint === 'ArtSci' ||
           endpoint === 'UT Institute of Agriculture';
}

function isTypeC(endpoint) {
    return endpoint === 'Campus Scene';
}

/* Fetches all articles of a certain endpoint */
function get(endpoint, update_articles=true) {
    console.log(endpoint);
    var url = endpoints[endpoint] + args;

    fetch(url).then((response) => {
        return response.json();
    }).then(async function (data) {
        // Builds in-memory article to be uploaded to database
        for (let datum in data) { console.log(datum); }
        for (let datum in data) {
            console.log(data[datum].title.rendered);
            const title = removeHTMLFromString(data[datum].title.rendered);
            const content = removeHTMLFromString(data[datum].content.rendered);
            const date = new Date(removeHTMLFromString(data[datum].date));
            let excerpt = removeHTMLFromString(data[datum].excerpt.rendered);
            let htmlContent = data[datum].content.rendered;
            
            let thumbnail = '';

            if (isTypeA(endpoint)) {
                thumbnail = data[datum].better_featured_image.source_url;
            } else if (isTypeB(endpoint)) {
                let thumbnail_url = data[datum]._links['wp:attachment'][0].href;

                await fetch(thumbnail_url).then((res) => {
                    return res.json();
                }).then((data) => {
                    if (data[0] && data[0].guid) {
                        thumbnail = data[0].guid.rendered;
                    } else {
                        thumbnail = '';
                    }
                });
            } else if (isTypeC(endpoint)) {
                let thumbnail_url = data[datum]._links['wp:featuredmedia'][0].href;
                await fetch(thumbnail_url).then((res) => {
                    return res.json();
                }).then((data) => {
                    if (data.source_url) {
                        thumbnail = data.source_url;
                    } else {
                        thumbnail = '';
                    }
                });
            } else { // Fallback
                thumbnail = data[datum].jetpack_featured_media_url;
                thumbnail = thumbnail ? thumbnail : "";
            }
            
            var regex = /Read More/gi;
            excerpt = excerpt.replace(regex, '');

            let article = {
                _id: data[datum].id,
                title: title,
                content: content,
                published: date,
                cataloged: new Date(Date.now()),
                thumbnail: thumbnail,
                excerpt: excerpt,
                url: data[datum].guid.rendered,
                source: endpoint,
                htmlContent: htmlContent
            };
            
            if (update_articles) {
                update(article);
            } else {
                console.log(`found article: (${endpoint}-${article._id}): {`);
                console.log(article);
                console.log('}');
            }
        }
    }).catch((error) => {
        console.log(url);
        console.log("Error:  " + error);
        return false
    });
}

// Returns whether given date is 9 months+ before today
function expires_soon(cataloged) {
    const nine_months = 23760000;
    const now = new Date();
    const diff = Math.round((now - cataloged) / 1000);
    return diff >= nine_months;
}

// Returns whether given date is 1 year+ before today
function expired(cataloged) {
    const year = 31536000;
    const now = new Date();
    const diff = Math.round((now - cataloged) / 1000);
    return diff >= year;
}

/*
    If the given article is already in the database:
        Update expiration flag if necessary,
        or delete if expired.
    Otherwise:
        Add the article to the database with document Id "Endpoint-UT Id"
*/
function update(article) {
    let docId = `${article.source}-${article._id}`;
    let docRef = db.collection('articles').doc(docId);
    console.log('updating...', docId);

    docRef.get().then((doc) => {
        if (doc.exists) {
            // if (expired(article.cataloged)) {
            //     docRef.delete().then(() => {
            //         console.log(`${docId} deleted.`);
            //     }).catch((error) => {
            //         console.log(`Error occurred when deleting ${docId}: ${error}.`);
            //     });
            // } else if (expires_soon(article.cataloged) && !article.expiring) {
            //     docRef.set({ 
            //         expiring: true 
            //     }, {
            //         merge: true
            //     }).then(() => {
            //         console.log(`Updated expiration for ${docId}.`);
            //     }).catch((error) => {
            //         console.log(`Error occurred: ${error}`);
            //     });   
            // }
            return true;
        } else {
            docRef.set({
                title: article.title,
                content: article.content,
                published: article.published,
                cataloged: article.cataloged,
                thumbnail: article.thumbnail,
                excerpt: article.excerpt,
                url: article.url,
                source: article.source,
                expiring: false,
                htmlContent: article.htmlContent,
            })
            .then(() => {
                console.log(`Article ${docId} successfully uploaded.`);
                return true;
            })
            .catch((error) => {
                console.error("Error adding document: ", error);
                return false;
            });        
        }
    }).catch((error) => {
        console.log(`Error occurred when uploading article: ${error}`);
        return false;
    });
}


// Initiate data processing...
// getall();
get('UT Institute of Agriculture', true);