let algoliasearch = require('algoliasearch');

var client = algoliasearch('LFBVCH9W02', 'f3fd9d1c0f827ac7f72bbb06be255f7b');
var index = client.initIndex('articles');

let to_fix = [];

index.search({
    query: '',
    hitsPerPage: 1000,
}, (err, content) => {
    console.log(`found ${content.nbHits}`)
    content.hits.forEach((hit) => {
        if (hit.published._seconds || hit.cataloged._seconds) {
            hit.published.seconds = hit.published._seconds;
            hit.published.nanoseconds = hit.published._nanoseconds;
            delete hit.published._seconds;
            delete hit.published._nanoseconds;

            hit.cataloged.seconds = hit.cataloged._seconds;
            hit.cataloged.nanoseconds = hit.cataloged._nanoseconds;
            delete hit.cataloged._seconds;
            delete hit.cataloged._nanoseconds;

            to_fix.push(hit);

            console.log(hit.published, hit.cataloged);
        }
    });

    index.partialUpdateObjects(to_fix, function(err, content) {
        if (err) throw err;
          
        console.log(content);
    });
});