const algoliasearch = require('algoliasearch');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fetch = require("node-fetch");
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

// Initialize Cloud Firestore through Firebase
admin.initializeApp();4
let db = admin.firestore();

const ALGOLyIA_ID = functions.config().algolia.app_id;
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key;
const ALGOLIA_SEARCH_KEY = functions.config().algolia.search_key;

const ALGOLIA_INDEX_NAME = 'articles';

const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

// Update the search index every time an article is written.
function saveToAlgolia(snap, context) {
  // Get the article document
  const article = snap.data();
  article.content = article.content.substring(0, 3300);

  if (article.htmlContent) {
    delete article.htmlContent;
  }

  let published = {};
  let cataloged = {};

  if (article.published && article.published._seconds) {
    published.seconds = article.published._seconds;
    published.nanoseconds = article.published._nanoseconds;
    article.published = published;
  }

  if (article.cataloged && article.cataloged._seconds) {
    cataloged.seconds = article.cataloged._seconds;
    cataloged.nanoseconds = article.cataloged._nanoseconds;
    article.cataloged = cataloged;
  }


  // Add an 'objectID' field which Algolia requires
  article.objectID = context.params.articleId;

  // Write to the algolia index
  const index = client.initIndex(ALGOLIA_INDEX_NAME);
  return index.saveObject(article);
}

function removeFromAlgolia(snap, context) {
  let articleId = context.params.articleId;
  const index = client.initIndex(ALGOLIA_INDEX_NAME);
  return index.deleteObject(articleId);
}

function updateDatabase() {
  return Promise.all([getall()]);
}

exports.onArticleCreated = functions.firestore.document('articles/{articleId}').onCreate((snap, context) => saveToAlgolia(snap, context));
exports.onArticleUpdated = functions.firestore.document('articles/{articleId}').onUpdate((change, context) => saveToAlgolia(change.after, context));
exports.onArticleDeleted = functions.firestore.document('articles/{articleId}').onDelete((snap, context) => removeFromAlgolia(snap, context));
exports.onTheHour = functions.pubsub.topic('hourly-tick').onPublish((message) => updateDatabase());


/* API endpoints where news articles can be found
   All use REST / JSON data, except UTM old which used XML.
   UTIA is not yet available.
*/
const endpoints = {
  "UT System": "https://news.tennessee.edu/wp-json/wp/v2/posts",
  "UTK": "https://news.utk.edu/wp-json/wp/v2/posts",
  "UTC": "https://blog.utc.edu/news/wp-json/wp/v2/posts",
  /*"UTM old" : "http://www.utm.edu/departments/univrel/archives/headlines_feed.php",*/
  "UTM": "https://www.utm.edu/news/wp-json/wp/v2/posts",
  "UTHSC": "https://news.uthsc.edu/wp-json/wp/v2/posts",
  /* "UTIA", */
  "IPS": "http://ips.tennessee.edu/wp-json/wp/v2/posts",
  'UTRF': "https://utrf.tennessee.edu/wp-json/wp/v2/posts",
  'Alumnus': 'https://alumnus.tennessee.edu/wp-json/wp/v2/posts',
  'Torchbearer': 'https://torchbearer.utk.edu/wp-json/wp/v2/posts',
  'CEHHS': 'https://cehhs.utk.edu/wp-json/wp/v2/posts',
  'Tickle': 'https://tickle.utk.edu/wp-json/wp/v2/posts',
  'ArchDesign': 'https://archdesign.utk.edu/wp-json/wp/v2/posts',
  'Nursing': 'https://nursing.utk.edu/wp-json/wp/v2/posts',
  'ArtSci': 'https://artsci.utk.edu/wp-json/wp/v2/posts',
  'Law': 'https://law.utk.edu/wp-json/wp/v2/posts',
  'Quest': 'https://quest.utk.edu/wp-json/wp/v2/posts',
  'Campus Scene': 'https://www.utm.edu/campusscene/wp-json/wp/v2/posts',
};
const num_results = 10;
const args = `?per_page=${num_results}`;

const html_regex = /<.+?>/g;
const newline_regex = /\n/g;
function removeHTMLFromString(str) {
  return entities.decode(str.replace(html_regex, '')).replace(newline_regex, '\n');
}

let articles = [] // serves as in-memory collation of articles

/* Fetches every article (by using get()) */
async function getall() {
  for (endpoint in endpoints) {
    await get(endpoint);
  }
  return 0;
}

function isTypeA(endpoint) {
  return endpoint === 'UT System' ||
    endpoint === 'Torchbearer' ||
    endpoint === 'Quest';
}

function isTypeB(endpoint) {
  return endpoint === 'IPS' ||
    endpoint === 'UTC' ||
    endpoint === 'Tickle' ||
    endpoint === 'ArchDesign' ||
    endpoint === 'Nursing' ||
    endpoint === 'ArtSci';
}

function isTypeC(endpoint) {
  return endpoint === 'Campus Scene';
}

/* Fetches all articles of a certain endpoint */
async function get(endpoint, update_articles = true) {
  console.log(endpoint);
  var url = endpoints[endpoint] + args;
  console.log('get(' + url + ')');

  await fetch(url).then((response) => {
    return response.json();
  }).then(async function (data) {
    // Builds in-memory article to be uploaded to database
    for (let datum in data) { console.log(datum); }
    for (let datum in data) {
      console.log(data[datum].title.rendered);
      const title = removeHTMLFromString(data[datum].title.rendered);
      const content = removeHTMLFromString(data[datum].content.rendered);
      const date = new Date(removeHTMLFromString(data[datum].date));
      let excerpt = removeHTMLFromString(data[datum].excerpt.rendered);
      let htmlContent = data[datum].content.rendered;

      let thumbnail = '';

      if (isTypeA(endpoint)) {
        thumbnail = data[datum].better_featured_image.source_url;
      } else if (isTypeB(endpoint)) {
        let thumbnail_url = data[datum]._links['wp:attachment'][0].href;

        await fetch(thumbnail_url).then((res) => {
          return res.json();
        }).then((data) => {
          if (data[0] && data[0].guid) {
            thumbnail = data[0].guid.rendered;
          } else {
            thumbnail = '';
          }
        });
      } else if (isTypeC(endpoint)) {
        let thumbnail_url = data[datum]._links['wp:featuredmedia'][0].href;
        await fetch(thumbnail_url).then((res) => {
          return res.json();
        }).then((data) => {
          if (data.source_url) {
            thumbnail = data.source_url;
          } else {
            thumbnail = '';
          }
        });
      } else {
        thumbnail = data[datum].jetpack_featured_media_url;
        thumbnail = thumbnail ? thumbnail : "";
      }

      var regex = /Read More/gi;
      excerpt = excerpt.replace(regex, '');

      let article = {
        _id: data[datum].id,
        title: title,
        content: content,
        published: date,
        cataloged: new Date(Date.now()),
        thumbnail: thumbnail,
        excerpt: excerpt,
        url: data[datum].guid.rendered,
        source: endpoint,
        htmlContent: htmlContent
      };

      if (update_articles) {
        await update(article);
      } else {
        console.log(`found article: (${endpoint}-${article._id}): {`);
        console.log(article);
        console.log('}');
      }
    }
  }).catch((error) => {
    console.log(url);
    console.log("Error:  " + error);
    return false
  });
}

// Returns whether given date is 9 months+ before today
function expires_soon(cataloged) {
  const nine_months = 23760000;
  const now = new Date();
  const diff = Math.round((now - cataloged) / 1000);
  return diff >= nine_months;
}

// Returns whether given date is 1 year+ before today
function expired(cataloged) {
  const year = 31536000;
  const now = new Date();
  const diff = Math.round((now - cataloged) / 1000);
  return diff >= year;
}

/*
  If the given article is already in the database:
      Update expiration flag if necessary,
      or delete if expired.
  Otherwise:
      Add the article to the database with document Id "Endpoint-UT Id"
*/
async function update(article) {
  let docId = `${article.source}-${article._id}`;
  let docRef = db.collection('articles').doc(docId);
  console.log('updating...', docId);

  await docRef.get().then((doc) => {
    if (doc.exists) {
      if (expired(article.cataloged)) {
        docRef.delete().then(() => {
          console.log(`${docId} deleted.`);
        }).catch((error) => {
          console.log(`Error occurred when deleting ${docId}: ${error}.`);
        });
      } else if (expires_soon(article.cataloged) && !article.expiring) {
        docRef.set({
          expiring: true
        }, {
            merge: true
          }).then(() => {
            console.log(`Updated expiration for ${docId}.`);
          }).catch((error) => {
            console.log(`Error occurred: ${error}`);
          });
      }
      console.log('article up to date!');
      return true;
    } else {
      docRef.set({
        title: article.title,
        content: article.content,
        published: article.published,
        cataloged: article.cataloged,
        thumbnail: article.thumbnail,
        excerpt: article.excerpt,
        url: article.url,
        source: article.source,
        expiring: false,
        htmlContent: article.htmlContent,
      })
        .then(() => {
          console.log(`Article ${docId} successfully uploaded.`);
          return true;
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
          return false;
        });
    }
  }).catch((error) => {
    console.log(`Error occurred when uploading article: ${error}`);
    return false;
  });
}
