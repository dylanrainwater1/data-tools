console.log(1);
const algoliasearch = require('algoliasearch');
const dotenv = require('dotenv');
const firebase = require('firebase');
require('firebase/firestore');

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyARwXjdXxlUFbPQCYRpQfBKkNHwyaImPkY',
    authDomain: 'aggregation-app.firebaseapp.com',
    projectId: 'aggregation-app'
});
var db = firebase.firestore();


// load values from the .env file in this directory into process.env
dotenv.load();
console.log(2);
console.log(3);
// configure algolia
const algolia = algoliasearch(
  process.env.ALGOLIA_APP_ID,
  process.env.ALGOLIA_API_KEY
);
const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);
console.log(4);

function updateAlgolia() {
  console.log('updating algolia');
  // Get all contacts from Firebase
  db.collection('articles').get().then((snapshot) => {
    // Build an array of all records to push to Algolia
    const records = [];

    snapshot.forEach((doc) => {
      // get the key and data from the snapshot
      const childData = doc.data();
      delete childData.htmlContent;

      childData.content = childData.content.substring(0, 3300);

      const childKey = doc.id;
      // We set the Algolia objectID as the Firebase .key
      childData.objectID = childKey;
      // Add object for indexing
      console.log(childKey, records.length, childData.content.length);
      records.push(childData);
    });

    // Add or update new objects
    index
      .saveObjects(records)
      .then(() => {
        console.log('Articles imported into Algolia');
      })
      .catch(error => {
        console.error('Error when importing article into Algolia', error);
        process.exit(1);
      });
  }).catch((err) => {
    console.error('Get error:', err);
  });
}

console.log(5);
updateAlgolia();
console.log(6);